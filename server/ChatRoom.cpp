#include "ChatRoom.h"

using namespace boost::asio::ip;

void ChatRoom::deliver(Message msg){
    recentMessages.push_back(msg);
    while (recentMessages.size() > maxRecentMessages)
      recentMessages.pop_front();

    for (auto user: users)
      user->deliver(msg);
}

void ChatRoom::join(std::shared_ptr<User> user){
  users.insert(user);
  std::cout<<"Client joined\n";
  for(auto msg : recentMessages){
    user->deliver(msg);
  }
}

void ChatRoom::leave(std::shared_ptr<User> user){
  std::cout<<"Client left\n";
  users.erase(user);
}
