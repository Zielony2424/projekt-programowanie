#include <iostream>
#include <boost/asio.hpp>
#include <boost/asio/steady_timer.hpp>
#include <string>
#include <chrono>
#include <vector>
#include <cstdlib>
#include "Message.h"
#include "Client.h"
#include "ChatServer.h"
#include "ChatSession.h"


int main(){
  boost::asio::io_service io;
  boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::tcp::v4(), 1339);
  boost::asio::ip::tcp::socket socket(io);
  std::cout<<"Ready...\n";
  ChatServer chatServer(io, endpoint);
  io.run();
}
