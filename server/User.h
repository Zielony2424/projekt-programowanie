#include "Message.h"
class User{
  public:
  virtual void deliver(const Message& msg)=0;
  virtual ~User(){};
};
