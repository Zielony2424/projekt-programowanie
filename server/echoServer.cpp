#include <iostream>
#include <boost/asio.hpp>
#include <string>
#include <array>
#include <optional>

using namespace boost::asio::ip;


constexpr size_t buffSize = 128;
std::array<char, buffSize> buffer;


void do_write(tcp::socket&, const size_t);

void do_read(tcp::socket& socket){
  socket.async_read_some(boost::asio::buffer(buffer, buffSize), 
    [&](const boost::system::error_code& error, size_t length){
      for(size_t i=0; i<length; ++i)
        std::cout<<buffer[i];
      if(error == boost::asio::error::eof){
        std::cout<<"Disconnected\n";
      }
      else{
        if(error)
          std::cerr<<"Error: "<<error.message()<<"\n";  
        do_write(socket, length);
      }
    });

}

void do_write(tcp::socket& socket, const size_t length){
  socket.async_write_some(boost::asio::buffer(buffer, length), 
    [&](const boost::system::error_code& error, size_t length){
      if(error)
        std::cerr<<"Error: "<<error.message()<<"\n";  
      do_read(socket);
    });
}

void acceptCallback(const boost::system::error_code& error, tcp::socket& socket){
  std::cout<<"Accepted\n";
  do_read(socket);
}

int main(){
  boost::asio::io_service io;
  tcp::acceptor acceptor(io, tcp::endpoint(tcp::v4(), 1337));
  tcp::socket socket(io);
  std::cout<<"Ready...\n";
  acceptor.listen();
  acceptor.async_accept(socket, [&](const boost::system::error_code& error){acceptCallback(error, socket);});
  io.run();
}

