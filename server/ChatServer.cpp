#include "ChatServer.h"

ChatServer::ChatServer(boost::asio::io_service& io, tcp::endpoint& endpoint):
  acceptor_(io, endpoint){
  do_accept(); 
}

void ChatServer::do_accept(){
  acceptor_.async_accept([this](boost::system::error_code ec, tcp::socket socket){
    if(!ec){
      std::make_shared<ChatSession>(std::move(socket), room_)->run();
      std::cout<<"Connected\n";
    }
    do_accept();
    });
  }

