#ifndef CHATSERVER_H
#define CHATSERVER_H
#include <boost/asio.hpp>
#include <iostream>
#include <utility>
#include "ChatSession.h"
#include "ChatRoom.h"

using namespace boost::asio::ip;

class ChatServer{
  tcp::acceptor acceptor_;
  ChatRoom room_;
  void do_accept();
  public:
  ChatServer(boost::asio::io_service& io, tcp::endpoint& endpoint);

};
#endif
