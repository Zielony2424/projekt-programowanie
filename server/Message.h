#ifndef MESSAGE_H
#define MESSAGE_H
#include <array>
#include <iostream>
#include <string>
#include <memory>
#include <cstdint>
//constexpr static int len = nickSize + bodSize + headSize;
class Message{//:public std::array<char, buffSize>{
  public:
  Message();
  Message(std::string nick, std::string body);
  size_t getBodySize() const;
  size_t length() const;
  bool decodeHeader();
  void encodeHeader(size_t bodySize);
  char* body();
  char* data();
  enum { maxBodySize = 512 };
  enum { headerSize = 4 };
  private:
  std::array<char, maxBodySize + headerSize> data_;
  int numberOfDigits(int i);
  std::string intToPaddedString(int i);
  size_t bodySize;
};
#endif

