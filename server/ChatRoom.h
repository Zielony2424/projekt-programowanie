#ifndef CHATROOM_H
#define CHATROOM_H
#include <set>
#include <boost/asio.hpp>
#include <utility>
#include "User.h"
#include "Message.h"
#include <iostream>
#include <memory>
#include <deque>


class ChatRoom{
  std::set<std::shared_ptr<User>> users;
  const int maxRecentMessages = 100;
  std::deque<Message> recentMessages;
  public:
  void deliver(Message);
  void join(std::shared_ptr<User>);
  void leave(std::shared_ptr<User>);
};
#endif
