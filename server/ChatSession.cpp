#include "ChatSession.h"

ChatSession::ChatSession(tcp::socket socket, ChatRoom& room):socket_(std::move(socket)), 
  room_(room){
  };

void ChatSession::run(){
  //do_readMessage(); 
  room_.join(shared_from_this());
  do_readBody();
}

void ChatSession::deliver(const Message& msg){
    bool writeInProgresss = !messagesToWrite.empty();
    messagesToWrite.push_back(msg);
    if (!writeInProgresss){
      do_write();
    }
}

void ChatSession::do_readHeader(){
  auto self(shared_from_this());
  boost::asio::async_read(socket_,
      boost::asio::buffer(message.data(), Message::headerSize),
      [this, self](boost::system::error_code ec, std::size_t length){
      if (!ec && message.decodeHeader()){
        do_readBody();
      }
      else{
        room_.leave(shared_from_this());
      }
    });
}

void ChatSession::do_readBody()
{
  auto self(shared_from_this());
  boost::asio::async_read(socket_,
      boost::asio::buffer(message.body(), message.getBodySize()),//message.bodySize),
      [this, self](boost::system::error_code ec, std::size_t /*length*/){
      if (!ec){
        room_.deliver(message);
        do_readHeader();
      }
      else{
        room_.leave(shared_from_this());
      }
    });
}
void ChatSession::do_write(){
    auto self(shared_from_this());
    boost::asio::async_write(socket_,
        boost::asio::buffer(messagesToWrite.front().data(),
          messagesToWrite.front().length()),
        [this, self](boost::system::error_code ec, std::size_t /*length*/){
          if (!ec){
            messagesToWrite.pop_front();
            if (!messagesToWrite.empty()){
              do_write();
            }
          }
          else{
            room_.leave(shared_from_this());
          }
      });
}

