#ifndef CHATSESSION_H
#define CHATSESSION_H
#include <boost/asio.hpp>
#include <utility>
#include "Message.h"
#include "ChatRoom.h"
#include <iostream>
#include <memory>
#include <deque>


using namespace boost::asio::ip;
class ChatSession: public std::enable_shared_from_this<ChatSession>,
public User{
  tcp::socket socket_;
  Message message;
  std::deque<Message> messagesToWrite;
  ChatRoom& room_;
  void do_readHeader();
  void do_readBody();
  void do_write();
  public:
  ChatSession(tcp::socket, ChatRoom&);
  void deliver(const Message&);
  void run();
};

#endif
