#!/usr/bin/env python3
from tkinter import *
import threading
import pexpect

def readMessage():
    while True:
        line = proc.readline().decode("utf-8)")
        if ":" in line: 
            messages.insert(INSERT, "\n" + line.rstrip())

def enter_pressed(event):
    input_get = input_field.get()
    proc.sendline(input_get)
    input_user.set('')
    return "break"

  
def runClient():
    global proc
    ip = inputIp.get()
    nick = inputNick.get()
    print("./client {} 1339".format(ip))
    proc = pexpect.spawn("./client {} 1339".format(ip))
    proc.delaybeforesend = None
    proc.timeout = None
    proc.sendline(nick)
    t = threading.Thread(name='readMessage', target=readMessage)
    t.start()
    popup.destroy()

#ip = input("Podaj ip: ")
window = Tk()
window.title("Chat")

popup = Toplevel(window)

labelIp = Label(popup, text="Podaj adress serwera.")
labelIp.grid(row=0, column=0)

inputIp = Entry(popup)
inputIp.grid(row=1, column=0)

labelNick = Label(popup, text="Podaj nick.")
labelNick.grid(row=2, column=0)

inputNick = Entry(popup)
inputNick.grid(row=3, column=0)

ok = Button(popup, text="Ok", command=runClient)
ok.grid(row=4, column=0)
cancel = Button(popup, text="Anuluj", command=window.destroy)
cancel.grid(row=4, column=1)

messages = Text(window)
messages.pack()

input_user = StringVar()
input_field = Entry(window, text=input_user)
input_field.pack(side=BOTTOM, fill=X)

frame = Frame(window)  # , width=300, height=300)
input_field.bind("<Return>", enter_pressed)
frame.pack()

window.mainloop()
