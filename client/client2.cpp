#include <iostream>
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <string>
#include <optional>

boost::array<char, 128> buffer;
std::string host="127.0.0.1";
int port=1337;
std::string message;
void receive(boost::asio::ip::tcp::socket& socket);
void send(boost::asio::ip::tcp::socket& socket,std::string host, int port, std::string message)
{
    //boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::address::from_string(host), port);
    //boost::asio::ip::tcp::socket socket(ios);
    //socket.connect(endpoint);
        //std::copy(message.begin(),message.end(),buffer.begin());
	//boost::system::error_code error;
	socket.async_write_some(boost::asio::buffer(buffer,message.size()),
	[&](const boost::system::error_code& error,int length){
	if (error == boost::asio::error::eof)
        //std::cerr<<"Error: "<<error.message()<<"\n";
        //return std::string();
        boost::asio::async_read_until(message, buffer, '\n',
        boost::bind(receive(socket),
        boost::asio::placeholders::error,
        boost::asio::placeholders::bytes_transferred));
        //receive(socket);
    });
}
void receive(boost::asio::ip::tcp::socket& socket)
{
    //boost::asio::ip::tcp::socket socket(ios);
    //boost::system::error_code error;
    socket.async_read_some(boost::asio::buffer(buffer, 128),
    [&](const boost::system::error_code& error, size_t length){
      for(size_t i=0; i<length; ++i)
        std::cout<<buffer[i];
      if(error == boost::asio::error::eof){
        std::cout<<"Disconnected\n";
      }
      else{
        if(error)
          std::cerr<<"Error: "<<error.message()<<"\n";
        send(socket,host,port,message);
      }
});
//return std::string(buffer.data());
}

int main()
{
     boost::asio::io_service ios;
     boost::asio::ip::tcp::socket socket(ios);
     socket.connect( boost::asio::ip::tcp::endpoint( boost::asio::ip::address::from_string(host), port ));
    //boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::address::from_string(host), port);
    //boost::asio::ip::tcp::socket socket(ios);
    //socket.connect(endpoint);
      //  std::copy(message.begin(),message.end(),buffer.begin());
	boost::system::error_code error;
    std::cin>>message;
    send(socket,host,port, message);
      ios.run();

    return 0;
}
