#include "Message.h"

Message::Message():bodySize(0){}

Message::Message(std::string nick, std::string body){
  std::string messageText = nick + ": " + body;
  encodeHeader(messageText.size());
  bodySize = messageText.size();
  for(auto i=0; i<messageText.size(); ++i){
    data_[i+headerSize] = messageText[i];
  }
}

int Message::numberOfDigits(int i){
  int noOfDigits = 0;
  while(i){
    noOfDigits++;
    i/=10;
  }
  return noOfDigits;
}

std::string Message::intToPaddedString(int i){
  auto digits = numberOfDigits(i);
  std::string result(headerSize - digits, '0');
  result += std::to_string(i);
  return result;
}

void Message::encodeHeader(size_t bodySize){
  auto header = intToPaddedString(bodySize);
  for(auto i=0; i<headerSize; ++i){
    data_[i] = header[i];
  }
}

bool Message::decodeHeader(){
  std::string header;
  for(int i=0; i<headerSize; ++i){
    header += data_[i];
  }
  try{
  bodySize = std::stoi(header);
  }
  catch(std::invalid_argument& e){
   bodySize = 0;
   //std::cerr<<"Error in decodeHeader: "<<e.what()<<"\n"<<header;
  }
  if(bodySize > maxBodySize){
    bodySize = 0;
    return false;
  }
  else
    return true;
}

size_t Message::getBodySize() const{
  return bodySize;
}

size_t Message::length() const{
  return bodySize + headerSize;
}

char* Message::body(){
  return data_.data() + headerSize;
}

char* Message::data(){
  return data_.data();
}
